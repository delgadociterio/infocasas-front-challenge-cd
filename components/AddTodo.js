import React, { Component } from 'react';
import PropTypes from 'prop-types';

export class AddTodo extends Component {
  state = {
    title: ''
  }

  onSubmit = (e) => {
    e.preventDefault();
    this.props.addTodo(this.state.title);
    this.setState({ title: ''});
  }

  onChange = (e) => this.setState({ [e.target.name]: e.target.value})

  render() {
    return (
      <div className="jumbotron jumbotron-fluid jumbo-form">
        <h2>Add a new task</h2>
        <form onSubmit={this.onSubmit} className="add-todo-form">
          <input className="add-todo" type="text" name="title" value={this.state.title} onChange={this.onChange}/>
          <input className="btn btn-success submit-todo" type="submit" value="Submit" disabled={this.state.title === ''}/>
        </form>
      </div> 
    )
  }
}

AddTodo.propTypes = {
  addTodo: PropTypes.func.isRequired
}

export default AddTodo
