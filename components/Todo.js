import React, { Component } from 'react';
import PropTypes from 'prop-types';

export class Todo extends Component {
  
  render() {
    const { id, title, completed } = this.props.todo;
    return (
      <li className="list-group-item">
        <input type="checkbox" checked={completed} onChange={this.props.completeTodo.bind(this, id)} />{' '}
        <span className={completed ? 'completed' : undefined}>{ title }</span>
        <button className="delete-todo" onClick={this.props.deleteTodo.bind(this, id)} disabled={this.props.isDisabled}>Delete</button>
        <button className="activate-edit" onClick={this.props.activateEdit.bind(this, id, title)} disabled={this.props.isDisabled}>Edit</button>
      </li>
    )
  }
}

Todo.propTypes = {
  todo: PropTypes.object.isRequired,
  completeTodo: PropTypes.func.isRequired,
  deleteTodo: PropTypes.func.isRequired,
  activateEdit: PropTypes.func.isRequired,
  isDisabled: PropTypes.bool.isRequired
}

export default Todo
