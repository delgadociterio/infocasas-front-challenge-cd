import Link from 'next/link'; 

const Navbar = () => (
  <nav className="navbar navbar-expand-lg navbar-dark fixed-top bg-primary mb-3">
    <a id="home-brand" className="navbar-brand" href="#">Infocasas Frontend Challenge</a>
  </nav>
);

export default Navbar;